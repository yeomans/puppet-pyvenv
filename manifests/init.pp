# $title: path at which to create virtual environment.
define pyvenv
    (
    String $owner,
    String $group,
    Optional[Boolean] $system_site_packages=undef,
    Optional[Boolean] $symlinks=undef, 
    Optional[Boolean] $copies=undef, 
    Optional[Boolean] $clear=undef, 
    Optional[Boolean] $upgrade=undef, 
    Optional[Boolean] $without_pip=undef, 
    String $requirements='',
    Hash $vpkgs={},
    )
    {
    realize Package['python3-venv']
    create_resources('@package', $vpkgs, {ensure => present}) 
        
    $pyenv_args = template('pyvenv/pyenv_args.erb')
    
    exec
        {
        "make_pyvenv_$title":
        command => "python3 -m venv $title && chown -R $owner:$group '$title'",
        path => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin',
        unless => "test -d $title",
        }~>
    exec
        {
        "upgrade_pip":
        command => "$title/bin/pip install --upgrade pip",
        refreshonly => true,
        }->  
    file
        {
        "$title/requirements.txt":
        ensure => file, 
        owner => "$owner", 
        group => "$group",
        mode => '0644',
        content => "$requirements",
        }->
    exec
        {
        "$title/bin/pip install --upgrade --requirement $title/requirements.txt":
        }    
    }
